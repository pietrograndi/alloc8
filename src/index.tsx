import React from 'react'
import ReactDOM from 'react-dom'
import { createGlobalStyle, ThemeProvider } from './style/styled'
import { theme } from './style/theme'
import { Main } from './components/pages/main'
import { ContextProvider } from './components/pages/mainContext'
import { generateDummyCollection } from './utils/data/dummydata'
import reset from 'styled-reset'

const GlobalStyle = createGlobalStyle`
  body {
    ${reset}
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,600&display=swap');
    margin: 0;
    padding: 0;
    background-color: #e5e5e5;
    font-family: 'Montserrat', sans-serif;
    font-weight: normal;
  }
`

const App = () => {
  const dummyData = generateDummyCollection(40)

  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <GlobalStyle />
        <ContextProvider list={dummyData}>
          <Main />
        </ContextProvider>
      </React.Fragment>
    </ThemeProvider>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
