import { Archive } from 'styled-icons/boxicons-regular/Archive'
import { Trash } from 'styled-icons/evil/Trash'
import { TimesCircle } from 'styled-icons/fa-solid/TimesCircle'
import { DragHandle } from 'styled-icons/material/DragHandle'
import { AddCircle } from 'styled-icons/material/AddCircle'
import { Restore } from 'styled-icons/material/Restore'
import { Menu } from 'styled-icons/material/Menu'
import { Sync } from 'styled-icons/octicons/Sync'
import { darken, lighten } from 'polished'
import styled from './styled'
import { theme } from './theme'

const icon = `
color: ${theme.canadaColors.dark_1};
height: 2.1rem;
transition: color 0.25s linear;
transition: transform 0.15s ease-out;
&:hover {
  transform: scale(1.1, 1.1);
  color: ${darken(0.2, theme.canadaColors.dark_1)};
  cursor: pointer;
}
`

export const RemoveIcon = styled(Trash)`
  ${icon}
`

export const ResetIcon = styled(TimesCircle)`
  ${icon}
  height: 1.5rem;
  color: ${lighten(0.1, theme.canadaColors.dark_1)};
  padding-right: 0.5rem;
  &:hover {
    color: ${theme.canadaColors.dark_1};
  }
`

export const ArchiveIcon = styled(Archive)`
  ${icon}
  height: 1.5rem;
`

export const DragHandleIcon = styled(DragHandle)`
  ${icon}
  height: 1.8rem;
`

export const AddIconCircle = styled(AddCircle)`
  height: 2.5rem;
`

export const MenuIcon = styled(Menu)`
  ${icon}
  position: absolute;
  top: 0.5rem;
  left: 0.5rem;
`

export const ReSortIcon = styled(Sync)`
  ${icon}
  position: absolute;
  top: 0.7rem;
  left: 50%;
`

export const ArchiveMenu = styled(Archive)`
  ${icon}
`

export const RestoreIcon = styled(Restore)`
  ${icon}
  height: 1.5rem;
`
