import _omit from 'lodash.omit'
import {
  ItemInterface,
  ObjectItemInterface,
  groupInterface,
  ObjectGroupInterface,
  ArchivedItemInterface,
} from '../interface_common'
import { ITEM_WEIGHT } from '../enums'
import { buildGroups, allocateItems } from './group_sorter'

export const getTotalWeight = (src: ItemInterface[]) => {
  return src.reduce((acc: number, elem: ItemInterface): number => {
    return acc + elem.weight
  }, 0)
}

export const filterCollectionByWeight = (src: ItemInterface[], itemWeight: ITEM_WEIGHT): ItemInterface[] => {
  return src.filter((item: ItemInterface) => item.weight === itemWeight)
}

export const getGroupWeightLimit = (src: ItemInterface[], groups: number): number => {
  const totalWeight = getTotalWeight(src)
  return Math.ceil(totalWeight / groups)
}

export const itemCanBeCombined = (group: ItemInterface[], limit: number, item: ItemInterface): boolean => {
  const newGroupSize = getTotalWeight(group) + item.weight
  return newGroupSize <= limit
}

export const parseListToLitteralObj = (list: ItemInterface[]): ObjectItemInterface => {
  return list.reduce((acc: ObjectItemInterface | {}, item: ItemInterface) => {
    return {
      ...acc,
      [item.id]: item,
    }
  }, {})
}

export function randomPick(total: ItemInterface[], processed: ItemInterface[]): ItemInterface {
  const processedIds = processed.map((item: ItemInterface) => item.id)
  const filtered = total.filter((item: ItemInterface) => processedIds.indexOf(item.id) === -1)
  const randomIndex = Math.floor(Math.random() * filtered.length)
  return filtered[randomIndex]
}

export function groupBlueprint(id: string, items: ItemInterface[]): groupInterface {
  return {
    id,
    name: 'gruppo',
    customName: '',
    items,
  }
}

export function filterGroupById(
  groups: ObjectGroupInterface,
  idGroupToRemove: string
): { restoreItems: ItemInterface[]; restGroups: ItemInterface[][] } {
  const restoreItems = groups[idGroupToRemove].items
  const restGroups = Object.values(groups)
    .filter((groups: groupInterface): boolean => groups.id !== idGroupToRemove)
    .map((groups: groupInterface): ItemInterface[] => groups.items)
  return {
    restoreItems,
    restGroups,
  }
}

function itemSplice(items: ItemInterface[], item: ItemInterface, index: number): ItemInterface[] {
  const start = items.slice(0, index)
  const end = items.slice(index, items.length)
  return [...start, item, ...end]
}

export function moveItem(
  groups: ObjectGroupInterface,
  obj: { dragIndex: number; hoverIndex: number; sourceId: string; destGroup: string }
) {
  const sourceItemList = groups[obj.sourceId].items
  const item = sourceItemList[obj.dragIndex]
  const updatedSource = sourceItemList.filter((item: ItemInterface, index: number) => index !== obj.dragIndex)

  const destination = obj.sourceId === obj.destGroup ? updatedSource : groups[obj.destGroup].items
  const updatedItemList = itemSplice(destination, item, obj.hoverIndex)

  return {
    [obj.sourceId]: {
      ...groups[obj.sourceId],
      items: updatedSource,
    },
    [obj.destGroup]: {
      ...groups[obj.destGroup],
      items: updatedItemList,
    },
  }
}

export function sortItems(
  src: ItemInterface[],
  startingGroup: ItemInterface[][],
  familyFirst: boolean = false
): ObjectGroupInterface {
  if (familyFirst) {
    const coupleList = filterCollectionByWeight(src, ITEM_WEIGHT.COUPLE)
    const singlesList = filterCollectionByWeight(src, ITEM_WEIGHT.SINGLE)
    const first = buildGroups(src, startingGroup, coupleList)
    const fisrtItemList = Object.values(first).map((group) => group.items)
    return buildGroups(src, fisrtItemList, singlesList)
  }
  return buildGroups(src, startingGroup)
}

export function removeGroupById(
  src: ItemInterface[],
  groups: ObjectGroupInterface,
  orderGroups: string[],
  idGroup: string
): { groups: ObjectGroupInterface; order: string[] } {
  const { restoreItems, restGroups } = filterGroupById(groups, idGroup)
  const newGroupOrder = orderGroups.filter((id: string) => id !== idGroup)
  const newItems = allocateItems(src, restGroups, restoreItems)
  const newGroupObj = newGroupOrder.reduce((acc, item: string, index: number) => {
    const newGroup = {
      [item]: {
        ...groups[item],
        items: newItems[index],
      },
    }
    return {
      ...acc,
      ...newGroup,
    }
  }, {})
  return {
    groups: newGroupObj,
    order: newGroupOrder,
  }
}

export function removeItemById(
  groups: ObjectGroupInterface,
  removeConfig: { idItem: string; idGroup: string }
): { removed: ItemInterface; rest: ItemInterface[] } {
  const group = groups[removeConfig.idGroup]
  const rest = group.items.filter((item: ItemInterface) => item.id !== removeConfig.idItem)
  const removed = group.items.filter((item: ItemInterface) => item.id === removeConfig.idItem)[0]
  return {
    rest,
    removed,
  }
}

export function counter(
  limit: number
): {
  count(): number
  reset(): void
  increment(): void
  check(): boolean
} {
  let counter: number = 0
  const reset = () => (counter = 0)
  const increment = () => (counter += 1)
  const check = () => counter === limit
  const count = () => counter
  return { count, reset, increment, check }
}

export function restoreArchivedItem(
  item: ArchivedItemInterface,
  archived: ArchivedItemInterface[],
  groups: ObjectGroupInterface
): { archived: ArchivedItemInterface[]; group: groupInterface } {
  const newItem = _omit(item, 'archivedFrom')
  const newArchived = archived.filter((archivedItem: ArchivedItemInterface) => archivedItem.id !== item.id)
  const currentItemsGroup = groups[item.archivedFrom.idGroup].items
  const newItemsGroup = itemSplice(currentItemsGroup, newItem, item.archivedFrom.index)
  return {
    archived: newArchived,
    group: {
      ...groups[item.archivedFrom.idGroup],
      items: newItemsGroup,
    },
  }
}

export function moveArchivedItem(
  obj: { idSrc: string; groupId: string; index: number },
  groups: ObjectGroupInterface,
  archived: ArchivedItemInterface[]
): { archived: ArchivedItemInterface[]; group: groupInterface } {
  const itemToMove = archived.filter((item: ArchivedItemInterface) => item.id === obj.idSrc)[0]
  const newArchived = archived.filter((item: ArchivedItemInterface) => item.id !== obj.idSrc)
  const currentItemsGroup = groups[obj.groupId].items
  const newItemsGroup = itemSplice(currentItemsGroup, itemToMove, obj.index)
  return {
    archived: newArchived,
    group: {
      ...groups[obj.groupId],
      items: newItemsGroup,
    },
  }
}
