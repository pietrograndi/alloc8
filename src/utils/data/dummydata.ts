import faker from 'faker'
import { ITEM_WEIGHT } from '../enums'
import { DummyItemInterface } from '../interface_common'

export const getDummyItem = (): DummyItemInterface => ({
  id: faker.random.uuid(),
  name: faker.name.firstName(),
  weight: faker.random.boolean() ? ITEM_WEIGHT.SINGLE : ITEM_WEIGHT.COUPLE,
})

export const generateDummyCollection = (n: number): DummyItemInterface[] =>
  new Array(n).fill('').map((): DummyItemInterface => getDummyItem())
