import {
  getTotalWeight,
  getGroupWeightLimit,
  itemCanBeCombined,
  parseListToLitteralObj,
  randomPick,
  groupBlueprint,
  filterGroupById,
  moveItem,
  filterCollectionByWeight,
  counter,
} from './group_utils'
import { ITEM_WEIGHT } from '../enums'
import { mockItem, mockItemList, mockGroup, mockObjectGroup } from './mocks'
import Deepequal from 'deep-equal'
import { ItemInterface } from '../interface_common'

describe('group utils', () => {
  it('getTotalWeight should return a total weight of an item list', () => {
    const total = getTotalWeight(mockItemList)
    expect(total).toEqual(5)
  })

  it('should filter a ItemInterface[] by Item.weight', () => {
    const filtered = filterCollectionByWeight(mockItemList, ITEM_WEIGHT.COUPLE)
    filtered.forEach((item: ItemInterface) => {
      expect(item.weight).toBe(ITEM_WEIGHT.COUPLE)
    })
  })

  it('should return a weight limit', () => {
    const limitPerGroup = getGroupWeightLimit(mockItemList, 2)
    expect(limitPerGroup).toEqual(3)
  })

  it('should check if the limit weight of a group is reached', () => {
    let limitReached = itemCanBeCombined(mockItemList, 5, mockItem)
    expect(limitReached).toBe(false)
    limitReached = itemCanBeCombined(mockItemList, 8, mockItem)
    expect(limitReached).toBe(true)
    limitReached = itemCanBeCombined(mockItemList, 7, mockItem)
    expect(limitReached).toBe(true)
    limitReached = itemCanBeCombined(mockItemList, 6, mockItem)
    expect(limitReached).toBe(false)
  })

  it('should turn a itemList in a litteral object', () => {
    const parsed = parseListToLitteralObj(mockItemList)
    mockItemList.forEach((item: ItemInterface, index: number) => {
      const isDeepEqual = Deepequal(parsed[item.id], item)
      expect(isDeepEqual).toBe(true)
    })
  })

  it('should return a random pick not aleray processed', () => {
    const processed: ItemInterface[] = [
      {
        name: 'ugo fantozzi',
        id: 'a0',
        weight: ITEM_WEIGHT.SINGLE,
        order: 0,
      },
    ]
    const pick = randomPick(mockItemList, processed)
    const serchInProcessed = processed.filter((item: ItemInterface) => item.id === pick.id)
    const searchInMockList = mockItemList.filter((item: ItemInterface) => item.id === pick.id)
    expect(searchInMockList.length).toBe(1)
    expect(serchInProcessed.length).toBe(0)
  })

  it('should create a groupBluePrint', () => {
    const newGroup = groupBlueprint('0', mockItemList)
    const isDeepEqual = Deepequal(mockGroup, newGroup)
    expect(isDeepEqual).toBe(true)
  })

  it('should filter group by id and return the new groups', () => {
    const { restoreItems, restGroups } = filterGroupById(mockObjectGroup, '1')

    const restoreItemsAreEqual = Deepequal(restoreItems, mockObjectGroup['1'].items)
    restGroups.forEach((items: ItemInterface[]) => {
      const isItemEquals = Deepequal(items, restoreItems)
      expect(isItemEquals).toBe(false)
    })
    expect(restoreItemsAreEqual).toBe(true)
  })
})

describe('moveItem function', () => {
  it('should return 2 groups with the itemList updated', () => {
    const conf = {
      dragIndex: 1,
      hoverIndex: 0,
      sourceId: '0',
      destGroup: '1',
    }
    const item = mockObjectGroup['0'].items[1]
    const srcLength = mockObjectGroup['0'].items.length
    const destLength = mockObjectGroup['1'].items.length
    const newObj = moveItem(mockObjectGroup, conf)
    const srcAfter = newObj['0'].items.length
    const destAfter = newObj['1'].items.length
    const isDeepEqual = Deepequal(newObj['1'].items[0], item)

    expect(srcLength > srcAfter).toBe(true)
    expect(destAfter > destLength).toBe(true)
    expect(isDeepEqual).toBe(true)
  })

  it('should return 1 group with a moved item', () => {
    const conf = {
      dragIndex: 2,
      hoverIndex: 0,
      sourceId: '0',
      destGroup: '0',
    }
    const item = mockObjectGroup[conf.sourceId].items[conf.dragIndex]
    const newObj = moveItem(mockObjectGroup, conf)
    const newItems = newObj[conf.destGroup].items[conf.hoverIndex]
    const isTheItemMoved = Deepequal(item, newItems)
    expect(
      Deepequal(
        mockObjectGroup[conf.sourceId].items[conf.dragIndex],
        mockObjectGroup[conf.destGroup].items[conf.hoverIndex]
      )
    ).toBe(false)
    expect(isTheItemMoved).toBe(true)
  })
})

describe('test counter', () => {
  it('should increment', () => {
    const limit = 2
    const { count, reset, increment, check } = counter(limit)
    let value = count()
    expect(value).toEqual(0)
    increment()
    value = count()
    expect(value).toEqual(1)
    increment()
    let reached = check()
    expect(reached).toBe(true)
    expect(count()).toEqual(limit)
    reset()
    expect(count()).toEqual(0)
  })
})
