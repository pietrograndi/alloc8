import { ITEM_WEIGHT } from '../enums'
import { ItemInterface, groupInterface, ObjectGroupInterface } from '../interface_common'

export const mockItem: ItemInterface = {
  id: '_',
  name: 'john doe',
  weight: ITEM_WEIGHT.COUPLE,
  order: 0,
}

export const mockItemList: ItemInterface[] = [
  {
    name: 'ugo fantozzi',
    id: 'a0',
    weight: ITEM_WEIGHT.SINGLE,
    order: 0,
  },
  {
    name: 'contessa serbelloni mazzanti',
    id: 'b1',
    weight: ITEM_WEIGHT.COUPLE,
    order: 1,
  },
  {
    name: 'rag filini',
    id: 'c2',
    weight: ITEM_WEIGHT.SINGLE,
    order: 2,
  },
  {
    name: 'geom. luciano calboni',
    id: 'd3',
    weight: ITEM_WEIGHT.SINGLE,
    order: 5,
  },
]

export const mockGroup: groupInterface = {
  id: '0',
  name: 'gruppo',
  items: mockItemList,
  customName: '',
}

export const mockObjectGroup: ObjectGroupInterface = {
  '0': mockGroup,
  '1': {
    id: '1',
    name: 'group 2',
    items: [
      {
        name: 'conte raffaello mascetti',
        id: 'c00',
        weight: ITEM_WEIGHT.COUPLE,
        order: 0,
      },
      {
        name: 'rambaldo melandri',
        id: 'c230',
        weight: ITEM_WEIGHT.SINGLE,
        order: 1,
      },
      {
        name: 'giorgio perozzi',
        id: 'c231',
        weight: ITEM_WEIGHT.SINGLE,
        order: 2,
      },
      {
        name: 'guido necchi',
        id: 'c236',
        weight: ITEM_WEIGHT.SINGLE,
        order: 2,
      },
      {
        name: 'prof. sassaroli',
        id: 'c239',
        weight: ITEM_WEIGHT.SINGLE,
        order: 2,
      },
    ],
    customName: '',
  },
}
