import faker from 'faker'
import { ItemInterface, ObjectGroupInterface } from '../interface_common'
import { getGroupWeightLimit, randomPick, itemCanBeCombined, groupBlueprint, counter } from './group_utils'

function reducer(
  groups: ItemInterface[][],
  pick: ItemInterface,
  lpg: number
): { groups: ItemInterface[][]; done: ItemInterface[] } {
  let done: ItemInterface[] = []
  groups = groups.sort((a: ItemInterface[], b: ItemInterface[]) => a.length - b.length)

  for (let i: number = 0; i < groups.length; i++) {
    if (itemCanBeCombined(groups[i], lpg, pick)) {
      const newGroup = [...groups[i], pick]
      groups[i] = newGroup
      done.push(pick)
      break
    }
  }
  return {
    groups,
    done,
  }
}

function loop(toProcess: ItemInterface[], groups: ItemInterface[][], limit: number) {
  const processed: ItemInterface[] = []
  const { reset, increment, check } = counter(groups.length)
  const maxLimit = limit + 1
  let newGroups: ItemInterface[][] = [...groups]
  while (processed.length < toProcess.length) {
    const lpg = check() ? maxLimit : limit
    const pick = randomPick(toProcess, processed)
    const { done, groups } = reducer(newGroups, pick, lpg)
    done.length === 0 ? increment() : reset()
    processed.push(...done)
    newGroups = groups
  }
  return newGroups
}

const groupBuilder = (items: ItemInterface[][]): ObjectGroupInterface => {
  return items.reduce((acc: ObjectGroupInterface, elem: ItemInterface[], index: number): ObjectGroupInterface => {
    const id = faker.random.uuid()
    const group = { [id]: groupBlueprint(id, elem) }
    return { ...acc, ...group }
  }, {})
}

export function allocateItems(
  src: ItemInterface[],
  groups: ItemInterface[][],
  elementToProcess = src
): ItemInterface[][] {
  const limitGroup = getGroupWeightLimit(src, groups.length)
  return loop(elementToProcess, groups, limitGroup)
}

export function buildGroups(
  src: ItemInterface[],
  groups: ItemInterface[][],
  elementToProcess = src
): ObjectGroupInterface {
  const sortedGroups = allocateItems(src, groups, elementToProcess)
  return groupBuilder(sortedGroups)
}
