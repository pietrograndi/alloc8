import { ITEM_WEIGHT } from './enums'

export interface ItemInterface {
  id: string
  name: string
  weight: ITEM_WEIGHT
  order: number
}

export interface ArchivedItemInterface extends ItemInterface {
  archivedFrom: {
    idGroup: string
    index: number
  }
}

export interface DummyItemInterface {
  id: string
  name: string
  weight: ITEM_WEIGHT
}
export interface groupInterface {
  id: string
  name: string
  customName: string
  items: ItemInterface[]
}

export interface ObjectItemInterface {
  [key: string]: ItemInterface
}

export interface ObjectGroupInterface {
  [key: string]: groupInterface
}
