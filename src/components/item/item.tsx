import React from 'react'
import styled from '../../style/styled'
import { theme } from '../../style/theme'
import { DragHandleIcon, ArchiveIcon, RestoreIcon } from '../../style/icons'
import { ITEM_WEIGHT } from '../../utils/enums'
import { darken, mix } from 'polished'
const singleColor = mix(0.5, theme.canadaColors.blue_1, '#ffffff')
const coupleColor = mix(0.5, theme.canadaColors.aquamarine_1, '#ffffff')

interface IWType {
  first: boolean
  last: boolean
  over: boolean
  dragging: boolean
  bg: string
}

const ItemWrapper = styled('div')<IWType>`
  display: flex;
  position: relative;
  padding: 10px;
  align-items: center;
  border-bottom: '1px solid white';
  background-color: ${(props) => props.bg};
  z-index: ${(props) => (props.dragging ? 2 : 1)};
  opacity: ${(props) => (props.dragging ? 0.5 : 1)};
  border-radius: 3px;
  border-width: 2px;
  border-style: solid;
  border-color: ${(props) => darken(0.4, props.bg)};
  transition: all 0.25s ease-out;
  margin-top: ${(props) => (props.over && !props.dragging ? '50px' : '0px')};

  .content {
    flex-grow: 1;
    color: ${darken(0.2, theme.canadaColors.dark_1)};
    label {
      font-size: 0.8rem;
    }
  }
  div {
    flex-grow: 0;
    margin-right: 10px;
  }
`

export interface ItemProps {
  isFirst: boolean
  isLast: boolean
  isDragging: boolean
  isOver: boolean
  weight: ITEM_WEIGHT
  name: string
  id: string
  handleRef: any
  restoreEnabled?: boolean
  archiveItem?(idItem: string): void
  restoreItem?(): void
}

export const Item = (props: ItemProps): JSX.Element => {
  return (
    <ItemWrapper
      first={props.isFirst}
      last={props.isLast}
      over={props.isOver}
      dragging={props.isDragging}
      bg={props.weight === ITEM_WEIGHT.COUPLE ? coupleColor : singleColor}
    >
      <div ref={props.handleRef}>
        <DragHandleIcon />
      </div>
      <div className="content">
        <h4>{props.name}</h4>
        <label>{props.weight === ITEM_WEIGHT.COUPLE ? 'couple' : 'single'}</label>
      </div>
      {typeof props.archiveItem !== 'undefined' && (
        <ArchiveIcon onClick={() => props.archiveItem && props.archiveItem(props.id)} />
      )}
      {typeof props.restoreItem !== 'undefined' && props.restoreEnabled && (
        <RestoreIcon onClick={() => props.restoreItem && props.restoreItem()} />
      )}
    </ItemWrapper>
  )
}

interface EmptyItemInterface {
  isOver: boolean
}

export const EmptyItem = styled('div')<EmptyItemInterface>`
  width: 100%;
  height: auto;
  min-height: 220px;
  background: ${(props) => (props.isOver ? mix(0.3, theme.canadaColors.dark_1, '#ffffff') : 'trasparent')};
  display: flex;
  align-content: center;
  align-items: center;
  p {
    width: 100%;
    text-align: center;
    color: ${(props) => (props.isOver ? theme.canadaColors.dark_1 : theme.canadaColors.dark_1)};
    font-weight: ${(props) => (props.isOver ? '600' : '500')};
  }
`
