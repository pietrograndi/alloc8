import React, { useRef, useContext } from 'react'
import { useDrop, useDrag, DropTargetMonitor, DragObjectWithType } from 'react-dnd'
import { Item, EmptyItem } from './item'
import { ItemInterface } from '../../utils/interface_common'
import { StoreCtx, StoreInterface } from '../pages/mainContext'

interface ItemDndProps {
  groupId: string
  index: number
  isLast: boolean
  isFirst: boolean
  item: ItemInterface
  move(obj: { dragIndex: number; hoverIndex: number; sourceId: string; destGroup: string }): void
  moveArchived(obj: { idSrc: string; groupId: string; index: number }): void
}

interface dropObj extends DragObjectWithType {
  id: string
  index: number
  groupIndex: string
  src: string
}

const ItemDnD = (props: ItemDndProps): JSX.Element => {
  const ref = useRef<HTMLDivElement>(null)
  const refInner = useRef<HTMLDivElement>(null)
  const { removeItem } = useContext(StoreCtx) as StoreInterface
  const [{ isOver }, drop] = useDrop({
    accept: 'card',
    drop(item: dropObj, monitor: DropTargetMonitor) {
      if (!ref.current) {
        return
      }
      const hoverIndex = props.index
      const destGroup = props.groupId

      if (item.src === 'GROUP') {
        const dragIndex = item.index
        const sourceId = item.groupIndex
        if (dragIndex === hoverIndex && sourceId === destGroup) return
        return props.move({ dragIndex, hoverIndex, sourceId, destGroup })
      }
      if (item.src === 'ARCHIVE') {
        return props.moveArchived({ idSrc: item.id, groupId: destGroup, index: hoverIndex })
      }
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
    }),
  })

  const [{ isDragging }, drag, preview] = useDrag({
    item: { type: 'card', id: props.item.id, index: props.index, groupIndex: props.groupId, src: 'GROUP' },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  preview(refInner)
  drop(ref)

  function archiveItem(idItem: string): void {
    removeItem({ idItem, idGroup: props.groupId, index: props.index })
  }

  const padding = props.isLast ? '10px' : '10px 10px 0px'
  return (
    <div ref={ref} style={{ padding }}>
      <div ref={refInner} style={{ borderRadius: '3px', backgroundColor: 'trasnparent' }}>
        <Item
          isOver={isOver}
          isDragging={isDragging}
          {...props.item}
          handleRef={drag}
          isFirst={props.isFirst}
          isLast={props.isLast}
          archiveItem={archiveItem}
        />
      </div>
    </div>
  )
}

export default ItemDnD

interface EmptyDropPropsInterface {
  groupId: string
  move(obj: { dragIndex: number; hoverIndex: number; sourceId: string; destGroup: string }): void
}

export const EmptyDrop = (props: EmptyDropPropsInterface): JSX.Element => {
  const ref = useRef<HTMLDivElement>(null)
  const [{ isOver }, drop] = useDrop({
    accept: 'card',
    drop(item: dropObj, monitor: DropTargetMonitor) {
      if (!ref.current) {
        return
      }
      const dragIndex = item.index
      const hoverIndex = 0
      const sourceId = item.groupIndex
      const destGroup = props.groupId
      return props.move({ dragIndex, hoverIndex, sourceId, destGroup })
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
    }),
  })

  drop(ref)

  return (
    <div ref={ref}>
      <EmptyItem isOver={isOver}>
        <p>{!isOver ? 'drag here an item' : 'drop to fill the group'}</p>
      </EmptyItem>
    </div>
  )
}
