import React, { ReactNode, useEffect, useState } from 'react'
import styled from '../../style/styled'
import { theme } from '../../style/theme'
import { lighten } from 'polished'

interface Sidebar {
  open: boolean
  children: ReactNode
}

const SideBarStyle = styled.div<Sidebar>`
  position: relative;
  background-color: ${lighten(0.4, theme.canadaColors.dark_1)};
  transition: all 0.25s ease-out;
  width: ${(props) => (props.open ? '20rem' : '0')};
  border-right: 2px solid;
  border-right-color: ${(props) => (props.open ? lighten(0.1, theme.canadaColors.dark_1) : 'transparent')};
`

export const SideBar = (props: Sidebar): JSX.Element => {
  const [fade, setFade] = useState(false)

  const fader = (arg: boolean) => {
    if (arg) {
      setTimeout(() => {
        setFade(arg)
      }, 250)
      return
    }
    setFade(arg)
  }

  useEffect(() => {
    fader(props.open)
  }, [props.open])

  return <SideBarStyle open={props.open}>{fade && <div>{props.children}</div>}</SideBarStyle>
}
