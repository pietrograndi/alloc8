import React, { useContext } from 'react'
import { Group, DropGroup } from '../group'
import { StoreCtx, StoreInterface } from './../pages/mainContext'
import { MenuIcon, ReSortIcon } from '../../style/icons'
import { ArchivedCounter } from '../archived'
import styled from '../../style/styled'

const QuickGrid = styled('div')`
  display: grid;
  position: relative;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  grid-template-rows: 1fr;
  grid-column-gap: 20px;

  grid-row-gap: 20px;
  padding-top: 5%;
  padding-bottom: 5%;
  padding-right: 5%;
  padding-left: 5%;
`

export const Viewer = (): JSX.Element => {
  const { groups, groupsOrder, toggleMenu, archivedItems, toggleArchive, setGroupsTo } = useContext(
    StoreCtx
  ) as StoreInterface
  return (
    <QuickGrid>
      <MenuIcon onClick={toggleMenu} />
      <ReSortIcon onClick={() => setGroupsTo(groupsOrder.length)} />
      {groupsOrder.map(
        (groupId: string, index: number): JSX.Element => {
          return <Group {...groups[groupId]} index={index} key={groupId} />
        }
      )}
      <DropGroup />
      <ArchivedCounter archived={archivedItems.length > 0} callback={toggleArchive} />
    </QuickGrid>
  )
}
