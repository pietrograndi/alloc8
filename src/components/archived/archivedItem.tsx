import React, { useRef } from 'react'
import { ArchivedItemInterface } from '../../utils/interface_common'
import { useDrag } from 'react-dnd'
import { Item } from '../item/item'

interface ItemArchiveProps {
  item: ArchivedItemInterface
  isRestoreEnabled: boolean
  isLast: boolean
  restoreItem(item: ArchivedItemInterface): void
}

export const ArchivedItem = (props: ItemArchiveProps): JSX.Element => {
  const refInner = useRef<HTMLDivElement>(null)
  const [{ isDragging }, drag, preview] = useDrag({
    item: { type: 'card', id: props.item.id, src: 'ARCHIVE' },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  })
  const padding = props.isLast ? '10px' : '10px 10px 0px'
  preview(refInner)
  return (
    <div style={{ padding, borderRadius: '3px', backgroundColor: 'trasnparent' }}>
      <div ref={refInner}>
        <Item
          isOver={false}
          isDragging={isDragging}
          {...props.item}
          handleRef={drag}
          isLast={props.isLast}
          isFirst={false}
          restoreEnabled={props.isRestoreEnabled}
          restoreItem={() => props.restoreItem(props.item)}
        />
      </div>
    </div>
  )
}
