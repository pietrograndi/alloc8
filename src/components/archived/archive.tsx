import React, { useContext } from 'react'
import { ArchivedItemInterface } from '../../utils/interface_common'
import { StoreCtx, StoreInterface } from '../pages/mainContext'
import { ArchivedItem } from './archivedItem'

interface ArchivePropsInterface {
  archivedItems: ArchivedItemInterface[]
}

export const Archive = (props: ArchivePropsInterface): JSX.Element => {
  const { groupsOrder, restoreItem } = useContext(StoreCtx) as StoreInterface

  return (
    <div>
      {props.archivedItems.length === 0 && <div>no archived items</div>}
      {props.archivedItems.map((item: ArchivedItemInterface, index: number) => {
        const restoreEnabled = groupsOrder.indexOf(item.archivedFrom.idGroup) !== -1
        return (
          <ArchivedItem
            isLast={index === props.archivedItems.length - 1}
            key={item.id}
            item={item}
            isRestoreEnabled={restoreEnabled}
            restoreItem={restoreItem}
          />
        )
      })}
    </div>
  )
}
