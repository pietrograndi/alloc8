import React from 'react'
import { ArchiveMenu } from '../../style/icons'
import styled from '../../style/styled'

interface archivedProps {
  archived: boolean
  callback(): void
}

const CounterWrapped = styled.div`
  position: absolute;
  top: 0.8rem;
  right: 1rem;
  > div {
    position: relative;
    &.active::after {
      display: block;
      position: absolute;
      right: -2px;
      top: -2px;
      width: 10px;
      height: 10px;
      background: red;
      border-radius: 10px;
      box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
      content: '';
    }
  }
`

export const ArchivedCounter = (props: archivedProps): JSX.Element => {
  return (
    <CounterWrapped>
      <div className={props.archived ? 'active' : ''}>
        <ArchiveMenu onClick={props.callback} />
      </div>
    </CounterWrapped>
  )
}
