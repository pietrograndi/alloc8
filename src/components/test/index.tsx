import React from 'react'
import styled from '../../style/styled'
import { color } from 'styled-system'

interface TestInderface {
  primary?: boolean
}

const TestDiv = styled('div')<TestInderface>`
  ${color}
`

export const Test = (): JSX.Element => {
  return (
    <TestDiv color="gold" primary>
      test component
    </TestDiv>
  )
}
