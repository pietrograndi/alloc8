import { ObjectGroupInterface, ItemInterface, ArchivedItemInterface } from '../../utils/interface_common'
import { sortItems } from '../../utils/data/group_utils'

interface StateInterface {
  groups: ObjectGroupInterface
  orderGroups: string[]
  src: ItemInterface[]
  archived: ArchivedItemInterface[]
  coupleFirst: boolean
  menu: boolean
  archiveMenu: boolean
}

interface IAction {
  type: string
  payload?: any
}

export const initalState = (list: ItemInterface[], groupNumber: number): StateInterface => {
  const startGroups = new Array(groupNumber).fill([])
  const groups = sortItems(list, startGroups)
  const orderGroups = Object.keys(groups)
  return {
    groups,
    orderGroups,
    src: list,
    archived: [],
    coupleFirst: false,
    menu: false,
    archiveMenu: false,
  }
}

// export const HANDLE_GROUP = 'HANDLE_GROUP'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const MOVE_ITEM = 'MOVE_ITEM'
export const RESTORE_ITEM = 'RESTORE_ITEM'
export const SET_GROUPS = 'SET_GROUPS'
export const REMOVE_GROUP = 'REMOVE_GROUP'
export const ADD_NEW_GROUP = 'ADD_NEW_GROUP'
export const TOGGLE_COUPLE_FIRST = 'TOGGLE_COUPLE_FIRST'
export const SET_CUSTOM_NAME = 'SET_CUSTOM_NAME'
export const TOGGLE_MENU = 'TOGGLE_MENU'
export const TOGGLE_ARCHIVE = 'TOGGLE_ARCHIVE'

export function reducer(state: StateInterface, action: IAction): StateInterface {
  switch (action.type) {
    case SET_CUSTOM_NAME:
      return {
        ...state,
        groups: {
          ...state.groups,
          [action.payload.idGroup]: {
            ...state.groups[action.payload.idGroup],
            customName: action.payload.customName,
          },
        },
      }
    case TOGGLE_ARCHIVE:
      return {
        ...state,
        archiveMenu: !state.archiveMenu,
      }
    case TOGGLE_MENU:
      return {
        ...state,
        menu: !state.menu,
      }
    case SET_GROUPS:
      return { ...state, groups: action.payload, orderGroups: Object.keys(action.payload) }
    case REMOVE_GROUP:
      return {
        ...state,
        groups: action.payload.groups,
        orderGroups: action.payload.order,
      }
    case REMOVE_ITEM: {
      return {
        ...state,
        groups: {
          ...state.groups,
          [action.payload.group.id]: action.payload.group,
        },
        archived: [...state.archived, action.payload.removed],
      }
    }
    case MOVE_ITEM:
      return {
        ...state,
        groups: { ...state.groups, ...action.payload },
      }
    case RESTORE_ITEM:
      return {
        ...state,
        groups: {
          ...state.groups,
          [action.payload.group.id]: action.payload.group,
        },
        archived: action.payload.archived,
      }
    case TOGGLE_COUPLE_FIRST:
      return {
        ...state,
        coupleFirst: !state.coupleFirst,
      }
    case ADD_NEW_GROUP:
      return {
        ...state,
        groups: { ...state.groups, [action.payload.id]: action.payload },
        orderGroups: [...state.orderGroups, action.payload.id],
      }
    default:
      throw new Error()
  }
}
