import React, { useState, useContext, useEffect } from 'react'
import { GroupCounter } from '../groupCounter'
import { Viewer } from '../viewer'
import { getGroupWeightLimit } from '../../utils/data/group_utils'
import { StoreCtx, StoreInterface } from './../pages/mainContext'
import HTML5Backend from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'
import { SideBar } from '../sidebar'
import styled from '../../style/styled'
import { Archive } from '../archived/archive'

const Wrapper = styled.div`
  display: flex;
  > div {
    flex-grow: 1;
  }
  > div:first-child,
  > div:last-child {
    flex-grow: 0;
  }
`

export const Main = (props: any): JSX.Element => {
  const {
    setGroupsTo,
    groupsOrder,
    src,
    toggleCoupleFirst,
    coupleFirst,
    menuIsOpen,
    archiveMenu,
    archivedItems,
  } = useContext(StoreCtx) as StoreInterface
  const [nextDisabled, setNextDisabled] = useState<boolean>(false)
  const checkNextdisabled = (): void => {
    const nextLimit = getGroupWeightLimit(src, groupsOrder.length) - 1 < 4
    setNextDisabled(nextLimit)
  }

  useEffect(() => {
    checkNextdisabled()
  })

  const handleGroupsNumber = (n: number): void => {
    if (n > 0) {
      setGroupsTo(n)
    }
  }

  return (
    <div>
      <DndProvider backend={HTML5Backend}>
        <Wrapper>
          <SideBar open={menuIsOpen}>
            <GroupCounter
              groupCount={groupsOrder.length}
              clickCallback={handleGroupsNumber}
              nextDisabled={nextDisabled}
              coupleFirst={coupleFirst}
              toggleCoupleFist={toggleCoupleFirst}
            />
          </SideBar>
          <Viewer />
          <SideBar open={archiveMenu}>
            <Archive archivedItems={archivedItems} />
          </SideBar>
        </Wrapper>
      </DndProvider>
    </div>
  )
}
