import React, { useReducer } from 'react'
import { ObjectGroupInterface, ItemInterface, ArchivedItemInterface } from '../../utils/interface_common'
import {
  moveItem,
  sortItems,
  removeGroupById,
  removeItemById,
  groupBlueprint,
  restoreArchivedItem,
  moveArchivedItem,
} from '../../utils/data/group_utils'
import {
  reducer,
  initalState,
  SET_GROUPS,
  REMOVE_ITEM,
  REMOVE_GROUP,
  MOVE_ITEM,
  TOGGLE_COUPLE_FIRST,
  ADD_NEW_GROUP,
  SET_CUSTOM_NAME,
  TOGGLE_MENU,
  TOGGLE_ARCHIVE,
  RESTORE_ITEM,
} from '../store/reducer'
import { random } from 'faker'

const initialGroupNumber = 9

export interface StoreInterface {
  groups: ObjectGroupInterface
  archivedItems: ArchivedItemInterface[]
  setGroupsTo(groups: number): void
  removeGroup(idGroup: string): void
  handleGroupName(idGroup: string, newTitle: string): void
  removeItem(obj: { idGroup: string; idItem: string; index: number }): void
  moveItem(obj: { dragIndex: number; hoverIndex: number; sourceId: string; destGroup: string }): void
  restoreItem(item: ArchivedItemInterface): void
  moveArchived(obj: { idSrc: string; groupId: string; index: number }): void
  toggleCoupleFirst(): void
  addNewGroup(): void
  toggleMenu(): void
  toggleArchive(): void
  menuIsOpen: boolean
  archiveMenu: boolean
  groupsOrder: string[]
  src: ItemInterface[]
  coupleFirst: boolean
}

export const StoreCtx = React.createContext<Partial<StoreInterface>>({})

export function ContextProvider(props: any): JSX.Element {
  const [state, dispatch] = useReducer(reducer, initalState(props.list, initialGroupNumber))

  const setGroupsTo = (n: number): void => {
    const startingGroup = new Array(n).fill([])
    const payload = sortItems(state.src, startingGroup, state.coupleFirst)
    dispatch({ type: SET_GROUPS, payload })
  }

  const removeGroup = (idGroup: string): void => {
    const payload = removeGroupById(state.src, state.groups, state.orderGroups, idGroup)
    dispatch({ type: REMOVE_GROUP, payload })
  }

  const handleGroupName = (idGroup: string, newTitle: string) => {
    dispatch({ type: SET_CUSTOM_NAME, payload: { idGroup, customName: newTitle } })
  }

  const removeItem = (obj: { idItem: string; idGroup: string; index: number }): void => {
    const { removed, rest } = removeItemById(state.groups, obj)
    const group = { ...state.groups[obj.idGroup], items: rest }
    const archiveItem = {
      ...removed,
      archivedFrom: {
        idGroup: obj.idGroup,
        index: obj.index,
      },
    }
    dispatch({ type: REMOVE_ITEM, payload: { removed: archiveItem, group } })
  }

  const setItems = (obj: { dragIndex: number; hoverIndex: number; sourceId: string; destGroup: string }): void => {
    const newObj = moveItem(state.groups, obj)
    dispatch({ type: MOVE_ITEM, payload: newObj })
  }

  const toggleCoupleFirst = () => {
    dispatch({ type: TOGGLE_COUPLE_FIRST })
  }

  const addNewGroup = () => {
    const group = groupBlueprint(random.uuid(), [])
    dispatch({ type: ADD_NEW_GROUP, payload: group })
  }

  const toggleMenu = () => {
    dispatch({ type: TOGGLE_MENU })
  }

  const toggleArchive = () => {
    dispatch({ type: TOGGLE_ARCHIVE })
  }

  const restoreItem = (item: ArchivedItemInterface) => {
    const payload = restoreArchivedItem(item, state.archived, state.groups)
    dispatch({ type: RESTORE_ITEM, payload })
  }

  const moveArchived = (obj: { idSrc: string; groupId: string; index: number }) => {
    const payload = moveArchivedItem(obj, state.groups, state.archived)
    dispatch({ type: RESTORE_ITEM, payload })
  }

  const storeValue = {
    groups: state.groups,
    groupsOrder: state.orderGroups,
    archivedItems: state.archived,
    src: state.src,
    moveItem: setItems,
    coupleFisrst: state.coupleFirst,
    menuIsOpen: state.menu,
    archiveMenu: state.archiveMenu,
    restoreItem,
    moveArchived,
    toggleCoupleFirst,
    toggleArchive,
    setGroupsTo,
    removeGroup,
    handleGroupName,
    removeItem,
    addNewGroup,
    toggleMenu,
  }

  return <StoreCtx.Provider value={storeValue}>{props.children}</StoreCtx.Provider>
}
