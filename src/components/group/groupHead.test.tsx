import React from 'react'
import ReactDOM from 'react-dom'

import { render, getByTestId, fireEvent } from '@testing-library/react'
import { GroupHead } from './groupHead'

const props = {
  id: '0000',
  title: 'test',
  removeGroup: jest.fn(),
  handleTitle: jest.fn(),
  customTitle: 'my custom title',
  noRemove: false,
}

describe('<GroupHead />', () => {
  it('should render properly', () => {
    const div = document.createElement('div')
    ReactDOM.render(<GroupHead title="test" customTitle="" {...props} />, div)
    ReactDOM.unmountComponentAtNode(div)
  })

  it('should render default title', () => {
    props.customTitle = ''
    const { container } = render(<GroupHead {...props} />)
    const title = getByTestId(container, 'title')
    expect(title.textContent).toBe(props.title)
  })

  it('should render customTitle if is present', () => {
    props.customTitle = 'custom'
    const { container } = render(<GroupHead {...props} />)
    const title = getByTestId(container, 'title')
    expect(title.textContent).toBe('custom')
  })

  it('should display an input when click the title and a reset input button', () => {
    const { getByTestId, rerender } = render(<GroupHead {...props} />)
    const title = getByTestId('title')
    fireEvent.click(title)
    const titleInput = getByTestId('titleInput')
    const resetBtn = getByTestId('resetBtn')
    expect(titleInput).toBeInTheDocument()
    expect(resetBtn).toBeInTheDocument()
  })

  it('should callback a dispatch group remove when click remove', () => {
    const { container } = render(<GroupHead {...props} />)
    const deleteButton = getByTestId(container, 'deleteBtn')
    fireEvent.click(deleteButton)
    expect(props.removeGroup).toHaveBeenCalled()
  })

  it('should render removeButton if noRemove = false', () => {
    props.noRemove = false
    const { container } = render(<GroupHead {...props} />)
    const deleteButton = getByTestId(container, 'deleteBtn')
    expect(deleteButton).toBeInTheDocument()
  })
})
