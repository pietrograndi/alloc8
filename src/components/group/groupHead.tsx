import React, { useState, useRef, useEffect } from 'react'
import OutsideClickHandler from 'react-outside-click-handler'
import styled from '../../style/styled'
import { theme } from '../../style/theme'
import { RemoveIcon, ResetIcon } from '../../style/icons'

const MyInputText = styled('input')`
  margin: 0;
  border: 0;
  padding: 0;
  flex-grow: 1;
  display: inline-block;
  vertical-align: middle;
  white-space: normal;
  background: none;
  line-height: 1;
  font-size: 15px;

  margin-right: 2rem;
  font-family: 'Raleway', sans-serif;
  font-weight: 500;
  color: ${theme.canadaColors.dark_1};
  border-bottom: 1px solid ${theme.canadaColors.dark_1};
  padding-bottom: 2px;
  &:focus {
    outline: none;
  }
`

const GroupHeadWrapper: React.FunctionComponent = styled.div`
  color: ${theme.canadaColors.dark_1};
  display: flex;
  padding: 15px;
  align-items: center;
  border-bottom: 2px dashed #ddd;
  .inputContent {
    width: 100%;
    > div {
      align-items: center;
      height: 2.5rem;
      display: flex;
    }
  }
  .description {
    align-items: center;
    display: flex;
    flex-grow: 1;
    height: 2.5rem;
    label {
      font-family: 'Raleway', sans-serif;
      font-weight: 700;

      &:hover {
        cursor: pointer;
      }
    }
  }
  .iconContent {
    flex-grow: 0;
  }
`

interface interfaceGroupHeadProps {
  id: string
  title: string
  removeGroup(): void
  handleTitle(newTitle: string): void
  customTitle: string
  noRemove: boolean
}

export const GroupHead = (props: interfaceGroupHeadProps): JSX.Element => {
  const [edit, setEdit] = useState(false)
  const [title, setTitle] = useState(props.customTitle ? props.customTitle : props.title)
  const inputRef = useRef<HTMLInputElement>(null)

  const setFocus = () => {
    if (!inputRef.current || !edit) {
      return
    }
    inputRef.current.focus()
  }

  useEffect(() => {
    setFocus()
  }, [edit])

  const handleTitle = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setTitle(e.target.value)
  }
  const clickOutside = () => {
    props.handleTitle(title)
    setEdit(false)
  }

  const pressEnter = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      props.handleTitle(title)
      setEdit(false)
    }
  }

  const resetInput = () => {
    setTitle('')
    setFocus()
  }

  return (
    <GroupHeadWrapper>
      {!edit && (
        <>
          <div className="description" onClick={() => setEdit(true)}>
            <label htmlFor={`inputText-${props.id}`} data-testid="title">
              {props.customTitle ? props.customTitle : props.title}
            </label>
          </div>
          <div className="iconContent" onClick={props.removeGroup} data-testid="deleteBtn">
            {!props.noRemove && <RemoveIcon />}
          </div>
        </>
      )}
      {edit && (
        <div className="inputContent">
          <OutsideClickHandler onOutsideClick={clickOutside}>
            <MyInputText
              id={`inputText-${props.id}`}
              data-testid="titleInput"
              type="text"
              ref={inputRef}
              onChange={handleTitle}
              onKeyDown={pressEnter}
              value={title}
            />
            <div className="iconContent" onClick={resetInput} data-testid="resetBtn">
              <ResetIcon />
            </div>
          </OutsideClickHandler>
        </div>
      )}
    </GroupHeadWrapper>
  )
}
