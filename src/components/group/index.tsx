import React, { useContext } from 'react'
import { lighten } from 'polished'
import { groupInterface, ItemInterface } from '../../utils/interface_common'
import { GroupHead } from './groupHead'
import { StoreCtx, StoreInterface } from './../pages/mainContext'
import { theme } from '../../style/theme'
import { AddIconCircle } from '../../style/icons'
import styled from '../../style/styled'
import ItemDnD, { EmptyDrop } from '../item/itemWrapper'

const { canadaColors } = theme

const GroupWrapper = styled('div')`
  box-sizing: border-box;
  color: ${canadaColors.dark_1};
  .innerContainer {
    border-radius: 4px;
    background-color: #f5f5f5;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }
`

interface groupProps extends groupInterface {
  index: number
}

export const Group = (props: groupProps): JSX.Element => {
  const { removeGroup, handleGroupName, groups, moveItem, moveArchived } = useContext(StoreCtx) as StoreInterface
  function deleteGroup(): void {
    removeGroup(props.id)
  }

  function setNewTittle(title: string): void {
    handleGroupName(props.id, title)
  }

  const hideRemove = Object.values(groups).length === 1
  const groupNumber = props.index + 1
  return (
    <GroupWrapper>
      <div className="innerContainer">
        <GroupHead
          id={props.id}
          title={props.customName ? props.customName : `group ${groupNumber}`}
          removeGroup={deleteGroup}
          handleTitle={setNewTittle}
          customTitle={props.customName}
          noRemove={hideRemove}
          data-testid="grouphead"
        />
        {props.items.length === 0 && <EmptyDrop groupId={props.id} move={moveItem} />}
        {props.items.map(
          (item: ItemInterface, index: number): JSX.Element => {
            return (
              <ItemDnD
                key={item.id}
                groupId={props.id}
                index={index}
                isFirst={index === 0}
                isLast={index === props.items.length - 1}
                move={moveItem}
                moveArchived={moveArchived}
                item={item}
              />
            )
          }
        )}
      </div>
    </GroupWrapper>
  )
}

interface GroupDropWrapper {
  noGroup?: boolean
}

const GroupDropWrapper = styled('div')<GroupDropWrapper>`
  display: flex;
  align-items: center;
  flex-direction: column;
  align-content: center;
  justify-content: center;
  div {
    display: flex;
    flex-direction: column;
    svg {
      transition: all 0.15s linear;
      color: ${lighten(0.1, theme.canadaColors.dark_1)};
    }
    p {
      transition: font-weight 0.15s linear;
      padding-top: 10px;
    }
  }
  border-radius: 4px;
  border: 2px dashed;
  border-color: ${canadaColors.gray_1};
  background-color: red;
  background: transparent;
  color: ${canadaColors.dark_1};
  transition: all 0.15s linear;
  height: ${(props) => (props.noGroup ? '300px' : 'auto')};
  min-height: 100%;
  &:hover {
    cursor: pointer;
    border-color: ${canadaColors.gray_2};
    div p {
      font-weight: 600;
    }
    div svg {
      transform: scale(1.1, 1.1);
      color: ${canadaColors.dark_1};
    }
  }
`

export const DropGroup = () => {
  const { addNewGroup } = useContext(StoreCtx) as StoreInterface

  return (
    <div onClick={() => addNewGroup()}>
      <GroupDropWrapper noGroup>
        <div>
          <AddIconCircle />
          <p>click to add a new group</p>
        </div>
      </GroupDropWrapper>
    </div>
  )
}
