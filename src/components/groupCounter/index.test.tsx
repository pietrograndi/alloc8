import React from 'react'
import ReactDOM from 'react-dom'
import { render, fireEvent } from '@testing-library/react'
import { GroupCounter } from './index'

const props = {
  groupCount: 1,
  clickCallback: jest.fn(),
  nextDisabled: false,
}

describe('<Group Counter />', () => {
  it('should render properly', () => {
    const div = document.createElement('div')
    ReactDOM.render(<GroupCounter {...props} />, div)
    ReactDOM.unmountComponentAtNode(div)
  })

  it('should render groupCounter properly', () => {
    const { container, getByTestId } = render(<GroupCounter {...props} />)
    expect(getByTestId('counter').textContent).toBe('1')
  })

  it('should dispatch an icrement and decrement value', () => {
    const { container, getByTestId, rerender } = render(<GroupCounter {...props} />)
    const increment = getByTestId('btn-increment')
    fireEvent.click(increment)
    expect(props.clickCallback).toHaveBeenCalledWith(props.groupCount + 1)
    const decrement = getByTestId('btn-decrement')
    rerender(<GroupCounter {...props} groupCount={2} />)
    fireEvent.click(decrement)
    expect(props.clickCallback).toHaveBeenCalledWith(1)
  })

  it('should disabled decrement props if counter is 0', () => {
    const { getByTestId } = render(<GroupCounter {...props} />)
    const decrement = getByTestId('btn-decrement')
    expect(decrement).toBeDisabled()
  })

  it('should disabled increment btn if nextDisabled is true', () => {
    props.nextDisabled = true
    const { getByTestId } = render(<GroupCounter {...props} />)
    const increment = getByTestId('btn-increment')
    expect(increment).toBeDisabled()
  })
})
