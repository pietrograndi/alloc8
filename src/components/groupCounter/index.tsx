import React from 'react'

interface counterInterface {
  nextDisabled: boolean
  groupCount: number
  clickCallback(newCount: number): void
  coupleFirst: boolean
  toggleCoupleFist(): void
}

export const GroupCounter = (props: counterInterface): JSX.Element => {
  return (
    <div>
      <div>
        <button
          data-testid="btn-decrement"
          disabled={props.groupCount === 1}
          onClick={() => props.clickCallback(props.groupCount - 1)}
        >
          -
        </button>
        <span data-testid="counter">{props.groupCount}</span>
        <button
          data-testid="btn-increment"
          disabled={props.nextDisabled}
          onClick={() => props.clickCallback(props.groupCount + 1)}
        >
          +
        </button>
      </div>
      <br />
      <label htmlFor="famFirst">
        <input type="checkbox" id="famFirst" checked={props.coupleFirst} onChange={props.toggleCoupleFist} />
        process couple first
      </label>
    </div>
  )
}
